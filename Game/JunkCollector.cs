﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using BeardedManStudios.Forge.Networking;
using System.Linq;

/// <summary>
/// Keeps track of GameItems and ConstructControllers in the scene that meet certain criterea. 
/// Gameitems must not have a parent
/// ConstructControllers must 4 parts or fewer
/// If their velocity hasn't changed for a set time, it is removed
/// </summary>
public class JunkCollector : MonoBehaviour
{
    #region WATCHED CLASSES
    [System.Serializable]
    class WatchedGameItemI
    {
        public GameItem gameItem;
        public float inactiveTimeSeconds;
        public Vector2 lastVelocity;
        public WatchedGameItemI(GameItem gameItem, float inactiveTimeSeconds = 0)
        {
            this.gameItem = gameItem;
            this.inactiveTimeSeconds = inactiveTimeSeconds;
            this.lastVelocity = gameItem.gameObject.GetComponent<Rigidbody2D>().velocity;
            
        }
    }
    
    [System.Serializable]
    class WatchedConstructControllerI
    {
        public ConstructControl constructControl;
        public Vector2 lastVelocity;
        public int lastPartCount;
        public float inactiveTimeSeconds;
        public WatchedConstructControllerI(ConstructControl constructControl, float inactiveTimeSeconds = 0)
        {
            this.constructControl = constructControl;
            this.lastVelocity = constructControl.gameObject.GetComponent<Rigidbody2D>().velocity;
            this.inactiveTimeSeconds = inactiveTimeSeconds;
            lastPartCount = constructControl.shipParts.Count;
        }
    }


    #endregion

    #region Variables
    [Header("Settings")]
    [Tooltip("Time in seconds something has to be inactive for to be destroyed")]
    private float MaxInactiveTimeGameItem = 600f;
    private float MaxInactiveTime = 300f;
    private float MaxInactiveTimeWithCockpit = 600f;
    [Header("All Watched Objects")]
    [SerializeField] List<WatchedGameItemI> WatchedGameItems;
    [SerializeField] List<WatchedConstructControllerI> WatchedConstructControllers;
    readonly float TimerIntervalSeconds = 1f;
    

    #endregion

    void Start()
    {
        if (NetworkManager.Instance.Networker is IClient)
            return;
        // SERVER CODE
        WatchedGameItems = new List<WatchedGameItemI>();
        WatchedConstructControllers = new List<WatchedConstructControllerI>();
        StartCoroutine(TimerCheckObjects());
    }

    IEnumerator TimerCheckObjects()
    {
        while (true)
        {
            yield return new WaitForSeconds(TimerIntervalSeconds);

            #region Update Watched lists with new objects
            // Game Items
            GameItem[] allGameItems = FindObjectsOfType<GameItem>();
            foreach (GameItem gameItem in allGameItems)
            {
                // add new ones
                if (!WatchedGameItems.Any(x => x.gameItem == gameItem))
                {
                    WatchedGameItems.Add(new WatchedGameItemI(gameItem));
                }
            }
            // Construct Controls
            ConstructControl[] allConstructControls = FindObjectsOfType<ConstructControl>();
            foreach (ConstructControl constructControl in allConstructControls)
            {
                // add new ones
                if (!WatchedConstructControllers.Any(x => x.constructControl == constructControl))
                {
                    WatchedConstructControllers.Add(new WatchedConstructControllerI(constructControl));
                }
            }
            #endregion

            // Remove any nulls
            WatchedGameItems.RemoveAll(item => item.gameItem == null);
            
            // loop backwards through both lists, update them, and networkDestroy & remove from list if needed
            // Game Items
            for (int i = WatchedGameItems.Count-1; i >= 0; i--)
            {
                WatchedGameItemI watchedGameItem = WatchedGameItems[i];
                if (watchedGameItem.gameItem == null)
                    continue;
                // add to inactive time if it hasn't moved, is off spawner, and has no parent
                if (watchedGameItem.gameItem.transform.parent == null && watchedGameItem.gameItem.isOnSpawner == false && watchedGameItem.lastVelocity == watchedGameItem.gameItem.gameObject.GetComponent<Rigidbody2D>().velocity)
                {
                    watchedGameItem.inactiveTimeSeconds += TimerIntervalSeconds;
                }
                // reset inactive time and last velocity if it has moved or is on spawner or it has a parent
                else
                {
                    watchedGameItem.inactiveTimeSeconds = 0;
                    watchedGameItem.lastVelocity = watchedGameItem.gameItem.gameObject.GetComponent<Rigidbody2D>().velocity;
                }
                // remove if inactive time is at threshold
                if (watchedGameItem.inactiveTimeSeconds >= MaxInactiveTimeGameItem)
                {
                    watchedGameItem.gameItem.networkObject.Destroy();
                    WatchedGameItems.RemoveAt(i);
                }

            }
            // Remove any nulls
            WatchedConstructControllers.RemoveAll(item => item.constructControl == null);
            // Construct Controllers
            for (int i = WatchedConstructControllers.Count - 1; i >= 0; i--)
            {
                WatchedConstructControllerI watchedCC = WatchedConstructControllers[i];
                if (watchedCC.constructControl == null) // skip nulls, they'll get removed next loop
                    continue;
                if (watchedCC.constructControl.GetDoNotDelete()) // Don't want to delete anything that has the flag
                    continue;
                
                var distFromCenter = Vector3.Distance(watchedCC.constructControl.transform.position, Vector3.zero);
                // add to inactive time if it hasn't had vel change and has <= 4 parts and the parts count hasn't changed, or is further than 100k units from world center and hasn't had vel change
                if ((watchedCC.constructControl.shipParts.Count <= 4 && watchedCC.lastVelocity == watchedCC.constructControl.gameObject.GetComponent<Rigidbody2D>().velocity && watchedCC.lastPartCount ==
                    watchedCC.constructControl.shipParts.Count) ||
                    watchedCC.lastVelocity == watchedCC.constructControl.gameObject.GetComponent<Rigidbody2D>().velocity && distFromCenter > 20000)
                {
                    watchedCC.inactiveTimeSeconds += TimerIntervalSeconds;
                }
                // Add to inactive time if the CC is empty, don't delete it right away because it might be in the process of spawning
                else if (watchedCC.constructControl.GetComponentsInChildren<PartBasic>(true).Length == 0)
                {
                    watchedCC.inactiveTimeSeconds += TimerIntervalSeconds;
                }
                // reset inactive time and last velocity if it has moved
                else
                {
                    watchedCC.inactiveTimeSeconds = 0f;
                    watchedCC.lastVelocity = watchedCC.constructControl.gameObject.GetComponent<Rigidbody2D>().velocity;
                    watchedCC.lastPartCount = watchedCC.constructControl.shipParts.Count;
                }
                // remove if inactive time is at threshold
                if ((watchedCC.inactiveTimeSeconds >= MaxInactiveTime && watchedCC.constructControl.GetComponentInChildren<ShipSeat>() == null) || 
                    (watchedCC.inactiveTimeSeconds >= MaxInactiveTimeWithCockpit && watchedCC.constructControl.GetComponentInChildren<ShipSeat>() != null))
                {
                    // if the CC has no children, we should just network destroy it
                    if (watchedCC.constructControl.GetComponentsInChildren<PartBasic>(true).Length == 0) // Empty CC, DELETE!!!
                    {
                        watchedCC.constructControl.networkObject.Destroy();
                        WatchedConstructControllers.RemoveAt(i);
                        continue;
                    }
                    List<PartBasic> AllShipPartsArray = new List<PartBasic>(watchedCC.constructControl.shipParts);
                    foreach (PartBasic pb in AllShipPartsArray)
                    {
                        if (pb == null)
                            continue;
                        pb.Server_ModifyHealth(-pb.GetMaxHealth(), true);
                    }
                    WatchedConstructControllers.RemoveAt(i);
                }
            }
        }
    }
}
