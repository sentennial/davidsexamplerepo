﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartBuilder : MonoBehaviour
{
    [SerializeField] string DisplayText = "";
    /// <summary>
    /// Mirror part if needed
    /// </summary>
    [SerializeField] GameObject MirrorPart;
    [SerializeField] GameObject BuildingCollider;
    [SerializeField] Sprite DisplaySprite;
    [SerializeField] private string TooltipFlavorText = "";
    [SerializeField] private List<string> TooltipStats;
    [System.Serializable]
    public class ResourceToBuild
    {
        public ResourceStorage.ResourceType rType;
        public _Resources.Resource resource;
        public float amountToTransact;
    }
    [SerializeField] private List<ResourceToBuild> resourcesToBuild = new List<ResourceToBuild>();
    public List<ResourceToBuild> getResourcesToBuild() { return resourcesToBuild; }
    public void SetResourceType()
    {
        foreach (ResourceToBuild rtm in resourcesToBuild)
        {
            rtm.resource = _Resources.GetResourceByID(rtm.rType.ToString());
        }
    }
    void Start()
    {
        SetResourceType();
    }

    /// <summary>
    /// Gets the mirrored part. If one isn't set, returns itself
    /// </summary>
    /// <returns></returns>
    public GameObject GetMirrorPart()
    {
        if (MirrorPart == null)
            return this.gameObject;
        else
            return MirrorPart;
    }
    public GameObject GetBuildingCollider()
    {
        return BuildingCollider;
    }
    public Sprite getDisplaySprite()
    {
        return DisplaySprite;
    }
    public bool HasMirrorPart()
    {
        if (MirrorPart == null)
            return false;
        else
            return true;
    }
    public string GetName()
    {
        return DisplayText;
    }
    public string GetTooltipFlavorText()
    {
        return TooltipFlavorText;
    }
    public List<string> GetTooltipCosts()
    {
        if (resourcesToBuild.Count == 0)
            return null;
        List<string> Costs = new List<string>();
        foreach (ResourceToBuild res in resourcesToBuild)
        {
            _Resources.Resource resource = _Resources.GetResourceByID(res.rType.ToString());
            Costs.Add("(" + res.amountToTransact + "): " + resource.rName);
        }

        return Costs;
    }
    public List<string> GetTooltipStats()
    {
        List<string> dynamicTooltipStats = new List<string>();

        PartBasic pb = GetComponent<PartBasic>();
        if (pb)
        {
            dynamicTooltipStats.Add("Health: " + pb.GetMaxHealth());
            dynamicTooltipStats.Add("Mass: " + pb.partMass);

        }
        ResourceStorage resource = GetComponent<ResourceStorage>();
        if (resource)
        {
            resource.SetResourceType();
            dynamicTooltipStats.Add(resource.GetResource().rName + ": " + resource.GetMaxResourceQuantity());
        }
        ProjectileLauncher launcher = GetComponent<ProjectileLauncher>();
        if (launcher)
        {
            dynamicTooltipStats.Add("Projectiles: " + launcher.projectiles.Count);
            dynamicTooltipStats.Add("Reload timer: " + launcher.getReloadTimerSeconds());

        }
        MagLock magLock = GetComponent<MagLock>();
        if (magLock)
        {
            dynamicTooltipStats.Add("Mag strength: " + Mathf.Abs(magLock.getMagneticLockStrength()));

        }
        ShipWeapon weapon = GetComponent<ShipWeapon>();
        if (weapon)
        {
            dynamicTooltipStats.Add("DPS: " + weapon.GetDamagePerShot());
        }
        Thruster thruster = GetComponent<Thruster>();
        Thruster thrusterbaby = GetComponentInChildren<Thruster>();
        if (thruster)
        {
            thruster.SetResourceType();
            dynamicTooltipStats.Add("Thrust pwr: " + thruster.thrusterPwr);
            foreach (Thruster.ResourceToModify rMod in thruster.GetResourcesToModify())
            {
                dynamicTooltipStats.Add("Resource: " + rMod.resource.rName + " - " + rMod.amountToTransact);
            }
        }
        else if (thrusterbaby)
        {
            thrusterbaby.SetResourceType();
            dynamicTooltipStats.Add("Thrust pwr: " + thrusterbaby.thrusterPwr);
            foreach (Thruster.ResourceToModify rMod in thrusterbaby.GetResourcesToModify())
            {
                dynamicTooltipStats.Add("Resource: " + rMod.resource.rName + " - " + rMod.amountToTransact);
            }
        }
        dynamicTooltipStats.AddRange(TooltipStats);

        return dynamicTooltipStats;
    }
}
