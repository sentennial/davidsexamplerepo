﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] int numInvSlots = 18;
    [SerializeField] int numHotbarSlots = 5;
    [Header("Prefabs")]
    [SerializeField] GameObject InventoryGrid;
    [SerializeField] TextMeshProUGUI StatTitle;
    [SerializeField] TextMeshProUGUI StatDescription;
    [SerializeField] GameObject HotbarGrid;
    [SerializeField] GameObject ItemPrefab;
    [SerializeField] GameObject TilePrefab;
    [SerializeField] GameObject CargoGrid;

    // CLIENT
    List<InventoryTile> tiles = new List<InventoryTile>();
    List<InventoryItem> items = new List<InventoryItem>();
    InventoryItem currentlySelectedItem = null;
    InventoryTile currentlyHoveredOverTile = null;
    List<InventoryItem> cargoItems = new List<InventoryItem>();
    List<InventoryTile> cargoTiles = new List<InventoryTile>();
    AvatarControl AC;

    // SERVER
    class ServerInventoryItem
    {
        public GameItem item { get; private set; }
        public int slot { get; set; }
        public ServerInventoryItem(GameItem item, int slot)
        {
            this.item = item;
            this.slot = slot;
        }
    }
    List<ServerInventoryItem> serverItems = new List<ServerInventoryItem>();

    public void Server_AddItem(GameItem itemToAdd, int index = -1)
    {
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();
        if (Application.isEditor) print("Server_AddItem " + itemToAdd.GetItemName());

        // Get a free slot number
        if (index < 0 || index > numInvSlots + numHotbarSlots || Server_isSlotFull(index))
        {
            ResourceStorage resourceStorageComponent = itemToAdd.GetComponent<ResourceStorage>();
            if (resourceStorageComponent) // Put all resources directly into the backpack.
            {
                index = Server_GetNextFreeBackpackSlot();
            }
            if (index == -1)
                index = Server_GetNextFreeSlot();
        }
        if (index == -1)
        {
            print("Tried adding item to full inventory, this shouldn't happen");
            if (PI) PI.Server_DropGameItem(itemToAdd);
            return;
        }
        // is item already in inventory somehow?
        if (Server_ItemExists(itemToAdd))
        {
            // resync inventory
            if (PI) PI.Server_SyncInventory();
        }
        else // slot isn't full, item isn't in inventory already
        {
            serverItems.Add(new ServerInventoryItem(itemToAdd, index));
            if (PI) PI.Server_SyncInventory();
        }

        
    }
    public string Server_GetInventoryAsString()
    {
        Server_RemoveNulls();
        string inventoryAsString = "";
        foreach (ServerInventoryItem serverInventoryItem in serverItems)
        {
            inventoryAsString += serverInventoryItem.slot + " " + serverInventoryItem.item.networkObject.NetworkId + " ";
        }
        
        return inventoryAsString.Trim();
    }

    void Server_RemoveNulls()
    {
        serverItems.RemoveAll(item => item.item == null);
    }

    bool Server_isSlotFull(int slot)
    {
        return serverItems.Find(item => item.slot == slot) != null;
    }
    int Server_GetNextFreeSlot()
    {
        for (int i = 0; i < numHotbarSlots+numInvSlots; i++)
        {
            if (!Server_isSlotFull(i))
                return i;
        }
        return -1;
    }
    int Server_GetNextFreeBackpackSlot()
    {
        for (int i = 5; i < numHotbarSlots + numInvSlots; i++)
        {
            if (!Server_isSlotFull(i))
                return i;
        }
        return -1;
    }
    bool Server_ItemExists(GameItem gameitem)
    {
        return serverItems.Find(item => item.item == gameitem) != null;
    }
    public void Server_RemoveItem(GameItem itemToRemove)
    {
        int i = serverItems.FindIndex(item => item.item == itemToRemove);
        if (i >= 0)
            serverItems.RemoveAt(i);
    }
    public bool Server_HasFreeSlots()
    {
        if (Application.isEditor) print("Server_HasFreeSlots " + serverItems.Count + "<"+ (numInvSlots + numHotbarSlots));
        return serverItems.Count < (numInvSlots + numHotbarSlots);
    }
    public List<GameItem> Server_GetAllGameItems()
    {
        List<GameItem> items = new List<GameItem>();
        foreach(ServerInventoryItem serverInventoryItem in serverItems)
        {
            items.Add(serverInventoryItem.item);
        }
        return items;
    }

    public void Server_MoveItemToNewSlot(GameItem gameItem, int newSlot)
    {
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();
        ServerInventoryItem currentServerItem = serverItems.Find(item => item.item == gameItem);
        // If they're trying to put it in a slot that already has an item, either swap or stack it
        if (Server_isSlotFull(newSlot))
        {
            if (Application.isEditor) print("Trying to swap or stack something");
            ServerInventoryItem destinationServerItem = serverItems.Find(item => item.slot == newSlot);

            // Determine if we want to stack resources, or swap items
            ResourceStorage fromStore = currentServerItem.item.GetComponent<ResourceStorage>();
            ResourceStorage toStore = destinationServerItem.item.GetComponent<ResourceStorage>();
            if (fromStore && toStore && fromStore.rType == toStore.rType)
            {
                float freeStorageInTarget = toStore.GetMaxResourceQuantity() - toStore.GetCurrentResourceQuantity();
                float qtyToPutInTarget = Mathf.Min(freeStorageInTarget, fromStore.GetCurrentResourceQuantity());

                fromStore.Server_ModifyCurrentResourceQuantity(-1*qtyToPutInTarget);
                toStore.Server_ModifyCurrentResourceQuantity(qtyToPutInTarget);
                if (fromStore.GetCurrentResourceQuantity() <= 0)
                {
                    PI.Server_DropAndDestroyGameItem(fromStore.GetComponent<ITEMResourceBox>());
                }
            }
            else
            {
                // swap items
                destinationServerItem.slot = currentServerItem.slot;
                currentServerItem.slot = newSlot;
                // Sync inventory to client
                PI.Server_SyncInventory();
            }
            return;
        }

        // If the game item isn't actually in their inventory, something is wrong, resync inventory
        if (!Server_ItemExists(gameItem))
        {
            PI.Server_SyncInventory();
            return;
        }

        // If we get here, item should exist in the inventory, and the newSlot shouldn't be full
        currentServerItem.slot = newSlot;
        PI.Server_SyncInventory();
    }
    
    public void Server_SplitStack(GameItem gameItem, int slotToSplitTo)
    {
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();
        if (Application.isEditor) print("Splitting " + gameItem.GetItemName() + " to " + slotToSplitTo);

        ResourceStorage storeToSplit = gameItem.GetComponent<ResourceStorage>();
        // If they're trying to split something other than a resourceStorage, stop
        if (!storeToSplit)
            return;
        // If slot to split to is full
        if (Server_isSlotFull(slotToSplitTo))
        {
            ResourceStorage storeToSplitTo = serverItems.Find(store => store.slot == slotToSplitTo).item.GetComponent<ResourceStorage>();
            // if they're trying to split to an existing store of the same type
            if (storeToSplitTo && storeToSplitTo.rType == storeToSplit.rType)
            {
                float freeStorageInTarget = storeToSplitTo.GetMaxResourceQuantity() - storeToSplitTo.GetCurrentResourceQuantity();
                float qtyToPutInTarget = Mathf.Min(freeStorageInTarget, storeToSplit.GetCurrentResourceQuantity()/2);

                storeToSplit.Server_ModifyCurrentResourceQuantity(-1 * qtyToPutInTarget);
                storeToSplitTo.Server_ModifyCurrentResourceQuantity(qtyToPutInTarget);
                if (storeToSplit.GetCurrentResourceQuantity() <= 0)
                {
                    PI.Server_DropAndDestroyGameItem(storeToSplit.GetComponent<ITEMResourceBox>());
                }
            }
        }
        else
        {
            float qtyToPutInTarget = storeToSplit.GetCurrentResourceQuantity() / 2;
            AC.Server_GiveResources(storeToSplit.rType, qtyToPutInTarget, specificSlot: slotToSplitTo);
            storeToSplit.Server_ModifyCurrentResourceQuantity(-1 * qtyToPutInTarget);
            if (storeToSplit.GetCurrentResourceQuantity() <= 0)
            {
                PI.Server_DropAndDestroyGameItem(storeToSplit.GetComponent<ITEMResourceBox>());
            }
        }
    }

    public GameItem Server_GetInventoryGameItem(int index)
    {
        ServerInventoryItem inventoryItem = serverItems.Find(i => i.slot == index);

        if (inventoryItem != null)
            return inventoryItem.item;
        else
            return null;
    }

    public void Server_RemoveAllInventoryItems()
    {
        serverItems.Clear();
    }

    public int Server_GetIndexOfGameItem(GameItem item)
    {
        ServerInventoryItem inventoryItem = serverItems.Find(i => i.item == item);
        if (inventoryItem != null)
            return inventoryItem.slot;
        else
            return -1;
    }

    // ============================================================== \\
    #region CLIENT
    bool isInitialized = false;
	void Awake ()
    {
        Client_InitializeInventory();
    }

    void Client_InitializeInventory()
    {
        if (isInitialized)
            return;
        AC = GetComponentInParent<AvatarControl>();
        isInitialized = true;
        for (int i = 0; i < numHotbarSlots; i++)
        {
            InventoryTile newTile = Instantiate(TilePrefab, HotbarGrid.transform, false).GetComponent<InventoryTile>();
            tiles.Add(newTile);
        }
        for (int i = 0; i < numInvSlots; i++)
        {
            tiles.Add(Instantiate(TilePrefab, InventoryGrid.transform, false).GetComponent<InventoryTile>());
        }
    }

    public void debug()
    {
        string s = "";
        s += "#tiles: " + tiles.Count + "\n";
        s += "#items: " + items.Count + "\n";
        for (int i = 0; i < tiles.Count; i++)
        {
            s += "i=" + i + "empty? " + tiles[i].isEmpty() + "GameItem: " + tiles[i].GetGameItem() + "\n";
        }

    }

    public void Client_SplitStack(InventoryItem itemToSplit)
    {
        int slotToSplitTo = tiles.IndexOf(currentlyHoveredOverTile);
        //Server_SplitStack(itemToSplit.GetGameItem(), slotToSplitTo);
        AC.GetPlayerInput().Client_SplitStack(itemToSplit.GetGameItem(), slotToSplitTo);

    }

    List<Coroutine> addItemCRs = new List<Coroutine>();
    public void SetInventoryFromString(string inventoryAsString)
    {
        if (Application.isEditor) print("setting I: " + inventoryAsString);
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();
        RemoveAllItems();
        string[] parsedInventory = inventoryAsString.Split(' ');
        if (parsedInventory.Length < 2 || parsedInventory.Length % 2 != 0)
            return;
        foreach (Coroutine cr in addItemCRs)
            StopCoroutine(cr);
        addItemCRs.Clear();
        for (int i = 0; i < parsedInventory.Length; i+=2)
        {
            int index = int.Parse(parsedInventory[i]);
            uint netID = uint.Parse(parsedInventory[i + 1]);
            addItemCRs.Add(StartCoroutine(FinishAddItem(PI, netID, tiles[index])));
        }
    }
    IEnumerator FinishAddItem(PlayerInput PI, uint netID, InventoryTile tile)
    {
        yield return new WaitUntil(() => PI.networkObject.Networker.NetworkObjects.ContainsKey(netID) == true);
        GameItem item = (GameItem)PI.networkObject.Networker.NetworkObjects[netID].AttachedBehavior;
        AddItem(item, tile);
        
    }
    public void AddItem(GameItem itemToAdd, InventoryTile tile = null)
    {
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();

        // Get free tile
        if (tile == null)
            tile = GetFreeTile();

        // Add new item prefab to the tile
        InventoryItem newInventoryItem = Instantiate(ItemPrefab, tile.transform, false).GetComponent<InventoryItem>();
        // Check Index of tile
        int index = tiles.FindIndex(i => i == tile);
        float tileSize = 60f;
        if (index > 4)
            tileSize = 80f;
        // Set up tile and item
        newInventoryItem.Setup(itemToAdd, tileSize);
        tile.AddItem(newInventoryItem);
        items.Add(newInventoryItem);
        if (PI.networkObject.IsServer && !PI.networkObject.IsOwner)
            PI.Server_SyncInventory();
    }
    void AddItem(GameItem itemToAdd, int index)
    {
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();
        if (tiles[index].isEmpty())
        {
            // Add new item prefab to the tile
            InventoryItem newInventoryItem = Instantiate(ItemPrefab, tiles[index].transform, false).GetComponent<InventoryItem>();
            // Set up tile and item
            newInventoryItem.Setup(itemToAdd, 60f);
            tiles[index].AddItem(newInventoryItem);
            items.Add(newInventoryItem);
            if (PI.networkObject.IsServer && !PI.networkObject.IsOwner)
                PI.Server_SyncInventory();
        }
    }

    public void Client_SelectHotbarItem(int index)
    {
        if (currentlySelectedItem)
            currentlySelectedItem.Deselect();

        if (tiles[index].GetInventoryItem())
        {
            tiles[index].GetInventoryItem().Select();
        }
        currentlySelectedItem = tiles[index].GetInventoryItem();
    }
    public void Client_UnselectHotbarItem()
    {
        if (currentlySelectedItem)
            currentlySelectedItem.Deselect();
        currentlySelectedItem = null;
    }
    
    void RemoveAllItems()
    {
        // delete all InventoryItems, not tiles
        foreach (InventoryItem item in items)
        {
            if (item && item.gameObject)
                Destroy(item.gameObject);
        }
        foreach (InventoryTile tile in tiles)
            tile.ClearItem();
        items.Clear();
    }
    
    //public List<InventoryTile> GetAllInventoryTiles()
    //{
    //    List<InventoryTile> itemsToReturn = new List<InventoryTile>(tiles);
    //    return itemsToReturn;
    //}
    

    InventoryTile GetFreeTile()
    {
        foreach (InventoryTile tile in tiles)
            if (tile.isEmpty())
                return tile;
        return null;
    }
    InventoryTile GetFreeBackpackTile()
    {
        for (int i = numHotbarSlots; i < numHotbarSlots + numInvSlots; i++)
        {
            if (tiles[i].isEmpty())
                return tiles[i];
        }
        return null;
    }
    void UpdateStatPanel()
    {
        if (currentlySelectedItem.GetGameItem() == null)
            return;
        StatTitle.text = currentlySelectedItem.GetGameItem().GetItemName();
        StatDescription.text = "Soon to be stats";
    }
    public void ItemWasClicked(InventoryItem itemClicked)
    {
        if (currentlySelectedItem)
            currentlySelectedItem.Deselect();
        itemClicked.Select();
        currentlySelectedItem = itemClicked;
        UpdateStatPanel();
    }
    public void ItemWasHoveredOver(InventoryTile tileHoveredOver)
    {
        currentlyHoveredOverTile = tileHoveredOver;
    }
    public void ClearHoveredOverTile()
    {
        currentlyHoveredOverTile = null;
    }

    public void ItemDragComplete(InventoryItem draggedItem)
    {

        if (draggedItem.getCurrentTile().cargoContainerRef && currentlyHoveredOverTile && currentlyHoveredOverTile.cargoContainerRef) // From Cargo to Cargo
        {
            if (Application.isEditor) print("Cargo->Cargo");
            //just put it back where it was
            draggedItem.transform.localPosition = Vector3.zero;
        }
        else if (draggedItem.getCurrentTile().cargoContainerRef != null) // From cargo to inventory
        {
            if (Application.isEditor) print("Cargo->Inv");
            int indexToMoveTo = tiles.IndexOf(currentlyHoveredOverTile);
            AC.GetPlayerInput().Client_CargoRemoveItem(draggedItem.getCurrentTile().cargoContainerRef, draggedItem.GetGameItem(), indexToMoveTo);
        }
        else if (currentlyHoveredOverTile && currentlyHoveredOverTile.cargoContainerRef != null) // From inventory to cargo
        {
            if (Application.isEditor) print("Inv->Cargo");
            if (!currentlyHoveredOverTile.isEmpty())
            {
                draggedItem.transform.localPosition = Vector3.zero;
                return;
            }
            AC.GetPlayerInput().Client_DropItem(draggedItem.GetGameItem());
            // tell cargo container to add item to inventory
            AC.GetPlayerInput().Client_CargoAddItem(currentlyHoveredOverTile.cargoContainerRef, draggedItem.GetGameItem());
        }
        else // from inventory to inventory
        {
            if (currentlyHoveredOverTile == null) // Not over an inventory tile
            {
                if (Application.isEditor) print("Inv->World");
                // Drop it
                AC.GetPlayerInput().Client_DropItem(draggedItem.GetGameItem());
            }
            else if (!currentlyHoveredOverTile.isEmpty()) // Inventory tile is not empty
            {
                if (Application.isEditor) print("Inv->FullSlot");
                // Reset the position
                draggedItem.transform.localPosition = Vector3.zero;
                // tell the server to either swap items or stack them
                int newIndex = tiles.FindIndex(tile => tile == currentlyHoveredOverTile);
                AC.GetPlayerInput().Client_MoveInvToNewTile(draggedItem.GetGameItem(), newIndex);
            }
            else // move to new slot
            {
                if (Application.isEditor) print("Inv->Inv");
                // Clear the item's current tile
                draggedItem.getCurrentTile().ClearItem();
                // Reparent the item to the new tile
                currentlyHoveredOverTile.AddItem(draggedItem);

                int newIndex = tiles.FindIndex(tile => tile == currentlyHoveredOverTile);
                AC.GetPlayerInput().Client_MoveInvToNewTile(draggedItem.GetGameItem(), newIndex);
            }
        }
    }

    

    #region CARGO
    Cargo currentCargoContainer;
    public void Client_InitializeCargo(int numSlots, int numColumns, Cargo cargoContainer, string cargoAsString)
    {
        if (Application.isEditor) print("setting C: " + cargoAsString);
        ClearCargo();
        CargoGrid.GetComponent<GridLayoutGroup>().constraintCount = numColumns;
        for (int i = 0; i < numSlots; i++)
        {
            InventoryTile newCargoTile = Instantiate(TilePrefab, CargoGrid.transform, false).GetComponent<InventoryTile>();
            newCargoTile.cargoContainerRef = cargoContainer;
            cargoTiles.Add(newCargoTile);
        }
        currentCargoContainer = cargoContainer;
        Client_SetCargoFromString(cargoAsString);
    }
    List<Coroutine> addCargoCRs = new List<Coroutine>();
    void Client_SetCargoFromString(string cargoString)
    {
        PlayerInput PI = GetComponentInParent<AvatarControl>().GetPlayerInput();
        string[] parsedInventory = cargoString.Split(' ');
        if (parsedInventory.Length < 2 || parsedInventory.Length % 2 != 0)
            return;
        foreach (Coroutine cr in addCargoCRs)
            StopCoroutine(cr);
        addCargoCRs.Clear();
        for (int i = 0; i < parsedInventory.Length; i += 2)
        {
            int index = int.Parse(parsedInventory[i]);
            uint netID = uint.Parse(parsedInventory[i + 1]);
            addCargoCRs.Add(StartCoroutine(FinishAddCargo(PI, netID, cargoTiles[index])));
        }
        
    }
    IEnumerator FinishAddCargo(PlayerInput PI, uint netID, InventoryTile tile)
    {
        yield return new WaitUntil(() => PI.networkObject.Networker.NetworkObjects.ContainsKey(netID) == true);
        GameItem item = (GameItem)PI.networkObject.Networker.NetworkObjects[netID].AttachedBehavior;
        AddCargoItem(item, tile);

    }
    void AddCargoItem(GameItem itemToAdd, InventoryTile tile)
    {

        // Add new item prefab to the tile
        InventoryItem newInventoryItem = Instantiate(ItemPrefab, tile.transform, false).GetComponent<InventoryItem>();
        // Set up tile and item
        newInventoryItem.Setup(itemToAdd);
        tile.AddItem(newInventoryItem);
        cargoItems.Add(newInventoryItem);
    }
    public void ClearCargo()
    {
        if (Application.isEditor) print("Clearing cargo");
        foreach (InventoryTile cargoTile in cargoTiles)
        {
            if(cargoTile)
                Destroy(cargoTile.gameObject);
        }
        foreach (InventoryItem cargoItem in cargoItems)
        {
            if(cargoItem)
                Destroy(cargoItem.gameObject);
        }
        cargoTiles.Clear();
        cargoItems.Clear();
    }
    #endregion
    #endregion
}
