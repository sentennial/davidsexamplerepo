﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DatabaseViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Key Bindings
        public static RoutedCommand cmd_NewItem = new RoutedCommand();
        public static RoutedCommand cmd_CloneItem = new RoutedCommand();
        public static RoutedCommand cmd_Close = new RoutedCommand();
        public static RoutedCommand cmd_Find = new RoutedCommand();
        // DB connection
        public static readonly string MyConString = "SERVER=10.1.10.6;" + "DATABASE=thermal_economy;" + "UID=staff;" + "PASSWORD=TeJl43!$;";

        public MainWindow()
        {
            InitializeComponent();
            checkDBConnection();
            updateTable();

            // Setup key bindings
            //New
            cmd_NewItem.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(cmd_NewItem, menu_NewItem));
            //Clone
            cmd_CloneItem.InputGestures.Add(new KeyGesture(Key.N, ModifierKeys.Control | ModifierKeys.Shift));
            CommandBindings.Add(new CommandBinding(cmd_CloneItem, menu_CloneItem));
            //Close
            cmd_Close.InputGestures.Add(new KeyGesture(Key.W, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(cmd_Close, closeApp));
            //Find
            cmd_Find.InputGestures.Add(new KeyGesture(Key.F, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(cmd_Find, searchFocus));

            //Check cutsheet folder
            if (System.IO.Directory.Exists(Properties.Settings.Default.PDF_top_folder_path) == false)
            {
                Properties.Settings.Default.PDF_top_folder_path = "";
                Properties.Settings.Default.Save();
            }
        }

        private void closeApp(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void updateTable()
        {
            String toSearch = t_search.Text;
            // If no search term
            if (toSearch.Length == 0)
                updateTableAll();
            else
                updateTableSearch(toSearch);

        }

        private void updateTableAll()
        {
            String sql = "SELECT * FROM thermal_economy.parts LIMIT 100";

            MySqlDataAdapter dataAdapter = doQuerry(sql);
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);

            // Set custom column names & widths
            setColumnNames(dataTable);

            dataGrid.DataContext = dataTable;

        }

        private void updateTableSearch(string toSearch)
        {
            // Construct query
            String sql = "SELECT * FROM thermal_economy.parts " +
                "WHERE (`item_number` REGEXP '" + toSearch + "') OR (`MPN` REGEXP '" + toSearch + "') OR (`description_short` REGEXP '" + toSearch + "') OR " +
                "(`description_long` REGEXP '" + toSearch + "') OR (`manufacturer` REGEXP '" + toSearch + "') OR (`vendor` REGEXP '" + toSearch + "') " +
                "LIMIT 100";
            // Run query
            MySqlDataAdapter dataAdapter = doQuerry(sql);
            // Fill dataGrid with results
            DataTable dataTable = new DataTable();
            dataAdapter.Fill(dataTable);
            setColumnNames(dataTable);
            dataGrid.DataContext = dataTable;
        }

        private static void setColumnNames(DataTable dataTable)
        {
            dataTable.Columns[0].ColumnName = "Item #";
            dataTable.Columns[1].ColumnName = "Short Description";
            dataTable.Columns[2].ColumnName = "Long Description";
            dataTable.Columns[3].ColumnName = "MPN";
            dataTable.Columns[4].ColumnName = "Price";
            dataTable.Columns[5].ColumnName = "Manufacturer";
            dataTable.Columns[6].ColumnName = "Vendor";
            dataTable.Columns[7].ColumnName = "Price Updated On";
        }

        private MySqlDataAdapter doQuerry(String querry)
        {
            using (MySqlConnection connection = new MySqlConnection(MyConString))
            {
                MySqlDataAdapter da;
                connection.Open();
                using (MySqlCommand cmdSel = new MySqlCommand(querry, connection))
                {
                    da = new MySqlDataAdapter(cmdSel);
                }
                connection.Close();
                return da;
            }
        }

        private void checkDBConnection()
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(MyConString))
                using (MySqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    Console.WriteLine("DB Connection: " + connection.State.ToString());
                    connection.Clone();
                }
            }
            catch (MySqlException mse)
            {
                MessageBox.Show("Error, cannot connect to database. Ensure you are on Thermal Economy's internal network, that the server is on, and that the database is running. \n\n" + mse.Message);
                this.Close();
            }
        }



        private void fillManufacturers(PartEditor pe)
        {
            String sql = "SELECT `manufacturer` FROM thermal_economy.parts GROUP BY `manufacturer`;";
            MySqlDataAdapter da = doQuerry(sql);
            DataTable dt = new DataTable(); da.Fill(dt);

            ObservableCollection<string> list = new ObservableCollection<string>();
            // Add Manufacturers to a list
            foreach (DataRow row in dt.Rows)
                list.Add(row[0].ToString());
            // Assign list to comboBox in window
            pe.Manufacturers = list;
        }

        private void fillPDFlinks(PartEditor pe)
        {
            String sql = "SELECT `path`, `updated_on` as 'Date_Updated' FROM thermal_economy.parts " +
                "LEFT JOIN thermal_economy.parts_pdfs " +
                "ON thermal_economy.parts.item_number = thermal_economy.parts_pdfs.item_number " +
                "WHERE thermal_economy.parts.item_number = '" + pe.ItemNumber + "';";
            MySqlDataAdapter da = doQuerry(sql);
            DataTable dt = new DataTable(); da.Fill(dt);

            pe.SetPDFlinks(dt);
    }

        private void searchFocus(object sender, RoutedEventArgs e)
        {
            t_search.Focusable = true;
            Keyboard.Focus(t_search);
        }

        // Event handlers
        private void rowDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            editSelectedRow();
        }

        private void rowDoubleClick(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                editSelectedRow();
            }
        }

        private void editSelectedRow()
        {
            // Get selected row
            DataRowView dataRow = (DataRowView)dataGrid.SelectedItem;

            //Quit if null
            if (dataRow == null)
                return;

            // Make a part editor
            var pe = new PartEditor();
            // Fill the part editor
            pe.ItemNumber = dataRow.Row.ItemArray[0].ToString();
            pe.DescShort = dataRow.Row.ItemArray[1].ToString();
            pe.DescLong = dataRow.Row.ItemArray[2].ToString();
            pe.MPN = dataRow.Row.ItemArray[3].ToString();
            pe.Man = dataRow.Row.ItemArray[5].ToString();
            pe.Vendor = dataRow.Row.ItemArray[6].ToString();
            pe.Price = dataRow.Row.ItemArray[4].ToString();
            pe.PriceUpOn = dataRow.Row.ItemArray[7].ToString();
            // Get manufacturers
            fillManufacturers(pe);
            // Get PDF links
            fillPDFlinks(pe);
            // Show the part editor
            pe.IsInit = true; // We're done setting up the editor
            try { pe.ShowDialog(); }
            catch (MySqlException mse) { MessageBox.Show("Error, changes were not saved. Make sure you entered the details correctly. \n\n" + mse.Message, "Error", MessageBoxButton.OK); pe.Close(); }

            // Refresh database view
            updateTable();
        }

        private void menu_Refresh_Click(object sender, RoutedEventArgs e)
        {
            updateTable();
        }

        private void searchEnterPressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                updateTable();
                e.Handled = true;
            }
        }

        private void b_clear_Click(object sender, RoutedEventArgs e)
        {
            t_search.Text = "";
            updateTable();
        }

        private void menu_NewItem(object sender, RoutedEventArgs e)
        {
            var pe = new PartEditor();
            pe.makeNewPart();

            // Get manufacturers
            fillManufacturers(pe);
            pe.IsInit = true;

            // Set next item number
            pe.ItemNumber = getNextItemNumber();

            // Show dialog
            try { pe.ShowDialog(); }
            catch (MySqlException mse) { MessageBox.Show("Error, changes were not saved. " + mse.Message + " " + mse.InnerException, "Error", MessageBoxButton.OK); pe.Close(); }

            // Refresh database view
            updateTable();
        }

        private String getNextItemNumber()
        {
            String nextItemNumber = "";
            String currentItemNumber = "";

            // Is a part selected? We will get the next item number based on that number.
            DataRowView dataRow = (DataRowView)dataGrid.SelectedItem;
            if (dataRow != null)
            {
                currentItemNumber = dataRow.Row.ItemArray[0].ToString();
                nextItemNumber = currentItemNumber.Substring(0, 6);
#if DEBUG
                Console.WriteLine("Next item number: " + nextItemNumber);
#endif
            }

            return nextItemNumber;
        }

        private void menu_CloneItem(object sender, RoutedEventArgs e)
        {
            var pe = new PartEditor();
            pe.makeNewPart();

            // Get manufacturers
            fillManufacturers(pe);
            // Copy details from selected part (if selected)
            DataRowView dataRow = (DataRowView)dataGrid.SelectedItem;
            if (dataRow != null)
            {
                // Fill the part editor
                pe.ItemNumber = dataRow.Row.ItemArray[0].ToString();
                pe.DescShort = dataRow.Row.ItemArray[1].ToString();
                pe.DescLong = dataRow.Row.ItemArray[2].ToString();
                pe.MPN = dataRow.Row.ItemArray[3].ToString();
                pe.Man = dataRow.Row.ItemArray[5].ToString();
                pe.Vendor = dataRow.Row.ItemArray[6].ToString();
                pe.Price = dataRow.Row.ItemArray[4].ToString();
                pe.PriceUpOn = dataRow.Row.ItemArray[7].ToString();
            }
            // We're done setting up the editor
            pe.IsInit = true;
            // Show the editor
            try { pe.ShowDialog(); }
            catch (MySqlException mse) { MessageBox.Show("Error, changes were not saved. " + mse.Message, "Error", MessageBoxButton.OK); pe.Close(); }

            // Refresh database view
            updateTable();
        }

        private void menu_Help(object sender, RoutedEventArgs e)
        {
            var helpWin = new HelpWindow();
            helpWin.ShowDialog();
        }

        private void menu_About(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This program was created by David Caron for use by Thermal Economy. \n\n" +
                "It requires the PC running it to be on the same network as the server and the server IP be 10.1.10.6");
        }

        private void menu_EditItem(object sender, RoutedEventArgs e)
        {
            editSelectedRow();
        }

        private void DataGridMenu_UpdateCost_Click(object sender, RoutedEventArgs e)
        {
            // Get selected row
            DataRowView dataRow = (DataRowView)dataGrid.SelectedItem;

            //Quit if null
            if (dataRow == null)
                return;

            String clip = Clipboard.GetText(TextDataFormat.Text);
            clip = clip.Trim();
            clip = Regex.Replace(clip, @"[$,]", "");
#if DEBUG
            Console.WriteLine("Price of item: " + dataRow.Row.ItemArray[4].ToString() + "\nPrice in clip: " + clip);
#endif
            try
            {
                // Run non-query
                using (MySqlConnection connection = new MySqlConnection(MyConString))
                using (MySqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                        command.CommandText = "UPDATE thermal_economy.parts SET " +
                                                "`price` = @tb_Price " +
                                                "WHERE `item_number` = @tb_ItemNum;";
                    
                    command.Parameters.AddWithValue("@tb_Price", clip);
                    command.Parameters.AddWithValue("@tb_ItemNum", dataRow.Row.ItemArray[0].ToString());

                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (MySqlException mse)
            {
                MessageBox.Show("Error, price not updated. " + mse.Message, "Error", MessageBoxButton.OK);
            }

            // Refresh database view
            updateTable();
        }

        private void menu_Settings_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingWindow = new SettingsWindow();
            settingWindow.ShowDialog();
        }
    }
}