﻿using Microsoft.Win32;
using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DatabaseViewer
{
    /// <summary>
    /// Interaction logic for PartEditor.xaml
    /// </summary>
    public partial class PartEditor : Window
    {
        // DB Connection
        //public static string MyConString = "SERVER=10.1.10.6;" + "DATABASE=thermal_economy;" + "UID=staff;" + "PASSWORD=TeJl43!$;";
        private bool isChanged = false; // If any data is changed in the form
        private bool isInit = false; // If the form has been initialized
        private bool isNewPart = false; // If the form is to create a new part instead of editing an existing one
        // Key Bindings
        public static RoutedCommand cmd_Save = new RoutedCommand();
        public static RoutedCommand cmd_Close = new RoutedCommand();
        public static RoutedCommand cmd_Delete = new RoutedCommand();
        // Item Number
        private string itemNumber;
        // List of PDF links
        private DataTable PDFlinks;

        public PartEditor()
        {
            InitializeComponent();
            // Setup key bindings
            //Save
            cmd_Save.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(cmd_Save, bn_Save_Click));
            //Close
            cmd_Close.InputGestures.Add(new KeyGesture(Key.W, ModifierKeys.Control));
            CommandBindings.Add(new CommandBinding(cmd_Close, bn_Close_Click));
            //Delete (we don't want this, delete is used for deleting a pdf link
            cmd_Delete.InputGestures.Add(new KeyGesture(Key.Delete));
            CommandBindings.Add(new CommandBinding(cmd_Delete, bn_Delete_Click));
            //PDF links table
            dg_PDFs.DataContext = PDFlinks;

            // Set readonly if in readonly mode
            if (Properties.Settings.Default.ReadonlyMode)
                setReadOnly();
        }

        private void Window_ContentRendered(object sender, RoutedEventArgs e)
        {
            dg_PDFs.Columns[0].Width = 500;
            dg_PDFs.Columns[1].Width = 50;
        }
        void setReadOnly()
        {
            this.Title += " SHOP MODE";
            Header_bg.Fill = System.Windows.Media.Brushes.Pink;
            Header_text.Content = "Shop Mode";
            tb_ItemNum   .IsReadOnly = true;
            tb_shDesc    .IsReadOnly = true;
            tb_lgDesc    .IsReadOnly = true;
            tb_MPN       .IsReadOnly = true;
            tb_Vendor    .IsReadOnly = true;
            tb_Price     .IsReadOnly = true;
            tb_PriceUpOn .IsReadOnly = true;
            bn_Delete.IsEnabled = false;
            bn_ChangeItemNum.IsEnabled = false;

        }

        public void makeNewPart()
        {
            bn_Delete.Visibility = Visibility.Hidden;
            bn_Close.Content = "Cancel";
            bn_ChangeItemNum.Visibility = Visibility.Hidden;
            tb_ItemNum.IsReadOnly = false;
            isNewPart = true;
        }

        private MySqlDataAdapter doQuerry(String querry)
        {
            using (MySqlConnection connection = new MySqlConnection(MainWindow.MyConString))
            {
                MySqlDataAdapter da;
                connection.Open();
                using (MySqlCommand cmdSel = new MySqlCommand(querry, connection))
                {
                    da = new MySqlDataAdapter(cmdSel);
                }
                connection.Close();
                return da;
            }
        }

        private void textBoxesChanged(object sender, TextChangedEventArgs e)
        {
            SetUnsavedState();
        }

        private void bn_Delete_Click(object sender, RoutedEventArgs e)
        {
            if (isNewPart) // Don't do anything if we're making a new part
                return;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete PN " + tb_ItemNum.Text + "?", "Really delete?", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                result = MessageBox.Show("Are you REALLY sure you want to delete PN " + tb_ItemNum.Text + "?", "Really really delete?", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    using (MySqlConnection connection = new MySqlConnection(MainWindow.MyConString))
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        connection.Open();
                        command.CommandText =   "DELETE FROM thermal_economy.parts " +
                                                "WHERE `item_number` = @itemnumber " +
                                                "LIMIT 1;";
                        command.Parameters.AddWithValue("@itemnumber", tb_ItemNum.Text);
                        command.ExecuteNonQuery();
                        connection.Close();
                    }

                    MessageBox.Show("Successfully deleted part.", "Success", MessageBoxButton.OK);

                    this.Close();
                }
            }
        }

        private void bn_Save_Click(object sender, RoutedEventArgs e)
        {
            // Check if the save button is active
            if (!bn_Save.IsEnabled)
                return;
            // Save part info only if we're not in read/write mode
            if (Properties.Settings.Default.ReadonlyMode == false)
                SavePartInfo();
            
            // Save pdf links
            SavePDFLinks();
            // Change state to saved
            isChanged = false;
            bn_Save.IsEnabled = false;
            this.Title = "Parts Editor";
            if (isNewPart)
                this.Close(); // Close editor if we added a new part
        }

        private void SavePartInfo()
        {
            // Run non-query
            using (MySqlConnection connection = new MySqlConnection(MainWindow.MyConString))
            using (MySqlCommand command = connection.CreateCommand())
            {
                connection.Open();
                if (!isNewPart) // Not a new part, just update existing part
                    command.CommandText = "UPDATE thermal_economy.parts SET " +
                                            "`description_short` = @tb_shDesc, " +
                                            "`description_long` = @tb_lgDesc, " +
                                            "`MPN` = @tb_MPN, " +
                                            "`price` = @tb_Price, " +
                                            "`manufacturer` = @cb_Man, " +
                                            "`vendor` = @tb_Vendor, " +
                                            "`item_number` = @tb_ItemNumNew " +
                                            "WHERE `item_number` = @tb_ItemNumOld;";
                else // Add a new part to the DB
                    command.CommandText = "INSERT INTO thermal_economy.parts " +
                                            "(`item_number`, `description_short`, `description_long`, `MPN`, `price`, `manufacturer`, `vendor`) " +
                                            "VALUES (@tb_ItemNum, @tb_shDesc, @tb_lgDesc, @tb_MPN, @tb_Price, @cb_Man, @tb_Vendor);";


                command.Parameters.AddWithValue("@tb_shDesc", tb_shDesc.Text);
                command.Parameters.AddWithValue("@tb_lgDesc", tb_lgDesc.Text);
                command.Parameters.AddWithValue("@tb_MPN", tb_MPN.Text);
                command.Parameters.AddWithValue("@tb_Price", tb_Price.Text.Replace(",", ""));
                command.Parameters.AddWithValue("@cb_Man", cb_Man.Text);
                command.Parameters.AddWithValue("@tb_Vendor", tb_Vendor.Text);
                command.Parameters.AddWithValue("@tb_ItemNumOld", itemNumber);
                command.Parameters.AddWithValue("@tb_ItemNumNew", tb_ItemNum.Text);
                command.Parameters.AddWithValue("@tb_ItemNum", tb_ItemNum.Text);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        private void SavePDFLinks()
        {

            using (MySqlConnection connection = new MySqlConnection(MainWindow.MyConString))
            {
                connection.Open();
                // Delete any PDF links associated with the Item Number
                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM thermal_economy.parts_pdfs WHERE item_number = @tb_ItemNum;";
                    command.Parameters.AddWithValue("@tb_ItemNum", tb_ItemNum.Text);

                    command.ExecuteNonQuery();
                }
                foreach (DataRow row in PDFlinks.Rows)
                {
                    if (row[0].ToString() == "")
                        continue;
                    // Insert each PDF links 
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO thermal_economy.parts_pdfs " +
                                                "(`item_number`, `path`, `updated_on`) " +
                                                "VALUES (@tb_ItemNum, @path, @updated);";
                        command.Parameters.AddWithValue("@tb_ItemNum", tb_ItemNum.Text);
                        command.Parameters.AddWithValue("@path", row[0].ToString());
                        command.Parameters.AddWithValue("@updated", row[1].ToString());

                        command.ExecuteNonQuery();
                    }
                    // Update all entries with the same path with the UpdatedDate. This ensures all references to the same doc have the same UpdatedDate
                    using (MySqlCommand command = connection.CreateCommand())
                    {
                        command.CommandText = "UPDATE thermal_economy.parts_pdfs set `updated_on` = @updated WHERE `path` = @path";
                        command.Parameters.AddWithValue("@path", row[0].ToString());
                        command.Parameters.AddWithValue("@updated", row[1].ToString());

                        command.ExecuteNonQuery();
                    }
                }
                
                connection.Close();
            }
            
        }

        private void bn_Close_Click(object sender, RoutedEventArgs e)
        {
            if (isChanged)
            {
                MessageBoxResult result = MessageBox.Show("There are unsaved changes, are you sure you want to exit the editor without saving?", "UNSAVED CHANGES", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                    this.Close();
            }
            this.Close();
        }

        private void bn_ChangeItemNum_Click(object sender, RoutedEventArgs e)
        {
            // Save old item number
            itemNumber = tb_ItemNum.Text;
            MessageBox.Show("Change the Item Number, then click Save.", "", MessageBoxButton.OK);

            tb_ItemNum.IsReadOnly = false;

        }

        public ObservableCollection<string> Manufacturers
        {
            set { cb_Man.ItemsSource = value; }
        }
        public String ItemNumber
        {
            get { return tb_ItemNum.Text; }
            set { tb_ItemNum.Text = value; itemNumber = value; }
        }
        public String DescShort
        {
            get { return tb_shDesc.Text; }
            set { tb_shDesc.Text = value; }
        }
        public String DescLong
        {
            get { return tb_lgDesc.Text; }
            set { tb_lgDesc.Text = value; }
        }
        public String MPN
        {
            get { return tb_MPN.Text; }
            set { tb_MPN.Text = value; }
        }
        public String Man
        {
            get { return cb_Man.Text; }
            set { cb_Man.Text = value; }
        }
        public String Vendor
        {
            get { return tb_Vendor.Text; }
            set { tb_Vendor.Text = value; }
        }
        public String Price
        {
            get { return tb_Price.Text; }
            set { tb_Price.Text = value; }
        }
        public String PriceUpOn
        {
            get { return tb_PriceUpOn.Text; }
            set { tb_PriceUpOn.Text = value; }
        }
        public bool IsInit { get => isInit; set => isInit = value; }
        public void SetPDFlinks(DataTable links)
        {
            PDFlinks = links;
            dg_PDFs.DataContext = PDFlinks;
        }
        public DataTable GetPDFlinks()
        {
            return PDFlinks.Clone();
        }
        private void dg_PDFs_Browse(object sender, MouseButtonEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb != null)
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(Properties.Settings.Default.PDF_top_folder_path + "\\" + tb.Text);
                openFileDialog.FileName = System.IO.Path.GetFileName(tb.Text);
                if (openFileDialog.ShowDialog() == true)
                {
                    tb.Text = openFileDialog.FileName;
                }
            }
        }        

        private void bn_NewPDFLink_Click(object sender, RoutedEventArgs e)
        {
            PDFlinks.Rows.Add("");
        }

        private void bn_DelPDFLink_Click(object sender, RoutedEventArgs e)
        {
            //Quit if nothing selected
            if (dg_PDFs.SelectedIndex == -1)
                return;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to remove the selected document reference? " +
                "This will not delete the actual file, only this part's reference to it.","", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                // Delete the selected row
                SetUnsavedState();
                PDFlinks.Rows.RemoveAt(dg_PDFs.SelectedIndex);
            }
                    
        }

        private void bn_EditPDFLink_Click(object sender, RoutedEventArgs e)
        {
            EditSelectedLink();
        }

        private void SetUnsavedState()
        {
            if (!isChanged && IsInit)
            {
                this.Title += "(CHANGED)";
                isChanged = true;
                bn_Save.IsEnabled = true;
            }
        }
        private void EditSelectedLink()
        {
            DataRowView dataRow = (DataRowView)dg_PDFs.SelectedItem;
            //Quit if null
            if (dataRow == null)
                return;

            // Quit if folder isn't set
            if (Properties.Settings.Default.PDF_top_folder_path == "")
            {
                MessageBox.Show("Error. You must set up where the main cutsheet folder is. Use the Options in the main window");
                return;
            }

            string localPath = dataRow.Row.ItemArray[0].ToString();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "All files (*.*)|*.*";
            string fullpath = Properties.Settings.Default.PDF_top_folder_path + "\\" + localPath;
            openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(fullpath);
            openFileDialog.FileName = System.IO.Path.GetFileName(fullpath);
            if (openFileDialog.ShowDialog() == true)
            {
                string link = openFileDialog.FileName;

                // Did they select something in the main PDF folder?
                if (link.Contains(Properties.Settings.Default.PDF_top_folder_path) == false)
                {
                    // If not, notify user
                    MessageBox.Show("Error. You must select a file in the directory " + Properties.Settings.Default.PDF_top_folder_path + "\n" +
                        "If this is not the correct folder, please change it.");

                }
                else // Otherwise, update the DataTable's row with the selected link
                {
                    link = link.Replace(Properties.Settings.Default.PDF_top_folder_path + "\\", "");
                    PDFlinks.Rows[dg_PDFs.SelectedIndex][0] = link;
                    //PDFlinks.Rows[dg_PDFs.SelectedIndex][1] = System.DateTime.Now.Month + "/" + System.DateTime.Now.Year;
                    SetUnsavedState();
                }
            }
        }

        private void bn_OpenFolderOfPDFLink_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRow = (DataRowView)dg_PDFs.SelectedItem;
            //Quit if null
            if (dataRow == null)
                return;
            string cell = dataRow.Row.ItemArray[0].ToString();
            string fullpath = Properties.Settings.Default.PDF_top_folder_path + "\\" + cell;
            if (System.IO.File.Exists(fullpath) == false)
            {
                MessageBox.Show("Error. That document no longer exists. Delete or edit it");
                return;
            }
            //string directory = System.IO.Path.GetDirectoryName(fullpath);
            string arg = "/select, \"" + fullpath + "\"";
            Process.Start("explorer.exe", arg);
        }

        private void bn_OpenPDFLink_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRow = (DataRowView)dg_PDFs.SelectedItem;
            //Quit if null
            if (dataRow == null)
                return;
            string cell = dataRow.Row.ItemArray[0].ToString();
            string fullpath = Properties.Settings.Default.PDF_top_folder_path + "\\" + cell;
            if (System.IO.File.Exists(fullpath) == false)
            {
                MessageBox.Show("Error. That document no longer exists. Delete or edit it");
                return;
            }

            Process.Start(@fullpath);
        }

        private void bn_SetDatePDFLink_Click(object sender, RoutedEventArgs e)
        {
            DataRowView dataRow = (DataRowView)dg_PDFs.SelectedItem;
            //Quit if null
            if (dataRow == null)
                return;

            MessageBoxResult result = MessageBox.Show("Note. This date will propagate to all parts with this document.", "", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                PDFlinks.Rows[dg_PDFs.SelectedIndex][1] = System.DateTime.Now.Month + "/" + System.DateTime.Now.Year;
                SetUnsavedState();
            }
        }

        private void dg_PDFs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dg_PDFs.SelectedIndex == -1) // Nothing selected
            {
                bn_DelPDFLink.IsEnabled = false;
                bn_EditPDFLink.IsEnabled = false;
                bn_OpenPDFLink.IsEnabled = false;
                bn_OpenFolderOfPDFLink.IsEnabled = false;
                bn_SetDatePDFLink.IsEnabled = false;
            }
            else // Something is selected
            {
                bn_DelPDFLink.IsEnabled = true;
                bn_EditPDFLink.IsEnabled = true;
                bn_OpenPDFLink.IsEnabled = true;
                bn_OpenFolderOfPDFLink.IsEnabled = true;
                bn_SetDatePDFLink.IsEnabled = true;
            }
        }
    }
}
