﻿Imports System.Data
Imports System.IO
Imports System.Reflection
Imports System.Windows.Threading
Imports Microsoft.Office.Interop
Imports MySql.Data.MySqlClient

Class MainWindow
    Public excel
    Public wb As Excel.Workbook
    Private Sub buttonChooseBOM_Click(sender As Object, e As RoutedEventArgs) Handles buttonChooseBOM.Click
        Dim dlg As New Microsoft.Win32.OpenFileDialog()
        ' Set filter for file extension and default file extension
        dlg.DefaultExt = ".xlsm"
        dlg.Filter = "BOMs (.xlsm)|*.xlsm"

        ' Display OpenFileDialog by calling ShowDialog method
        Dim result As Nullable(Of Boolean) = dlg.ShowDialog()

        ' Get the selected file name and display in a TextBox
        If result = True Then
            ' Open document
            Dim filename As String = dlg.FileName
            textBoxChooseBOM.Text = filename
        End If
    End Sub

    Private Sub buttonGenerate_Click(sender As Object, e As RoutedEventArgs) Handles buttonGenerate.Click
        If Not Directory.Exists(My.Settings.PDFfolder) Then
            MsgBox("Error, Your top-level documentation folder is not set correctly. Please set in settings prior to generating a manual")
        End If
        Try
            excel = New Excel.Application()
            buttonGenerate.IsEnabled = False
            buttonChooseBOM.IsEnabled = False
            updateStatus("Generating O&M Manual...")
            generateOMManual()
            updateStatus("Finished")

            'Close Excel and the BOM
            wb.Close(False)
            excel.Quit()
        Catch ex As Exception
            gracefulClose()
            MsgBox("There was an issue and the program had to close. Please contact David if the problem persists. " + ex.Message + " - " + ex.Data.ToString)
        Finally
            excel.quit()
            releaseObject(excel)
        End Try
    End Sub

    Public Sub gracefulClose()
        If wb IsNot Nothing Then
            'wb.Close(False)
        End If
        If excel IsNot Nothing Then
            excel.Quit()
            releaseObject(excel)
        End If
    End Sub

    Public Sub updateStatus(ByVal status As String)
        textBoxGenerateProgress.AppendText(status)
        textBoxGenerateProgress.AppendText(Environment.NewLine)
        textBoxGenerateProgress.ScrollToEnd()
        AllowUIToUpdate()
    End Sub

    Private Sub generateOMManual()
        If My.Settings.PDFfolder Is "" Then
            updateStatus("Error. You need to tell me where to find all the PDFs")
        End If
        Dim progressIncriment As Decimal
        'Create folder for manual
        Dim curDir As String = Directory.GetCurrentDirectory()
        Dim manualDestDir As String = curDir + "\GeneratedManual"
        'Dim manualSrcFilesDir As String = curDir + "\ManualFiles"
        Dim manualSrcFilesDir As String = My.Settings.PDFfolder ' look for PDFs in the defined folder
        Directory.CreateDirectory(manualDestDir)

        'Analyze BOM
        wb = openBOM(excel)
        Dim wsControlSystem = getWS(excel, "Control System")
        Dim row As Integer = getPartNumberRow(wsControlSystem)
        Dim colPartNumber As Integer = getPartNumberColumn(wsControlSystem)
        Dim colManufacturer As Integer = getManufacturerColumn(wsControlSystem)
        Dim colItemNumber As Integer = getItemNumberColumn(wsControlSystem)
        progressIncriment = 1.3

        updateStatus("Reading parts from BOM...")
        While wsControlSystem.Cells(row, colPartNumber).value IsNot Nothing And wsControlSystem.Cells(row, colManufacturer).value IsNot Nothing

            Dim part = wsControlSystem.Cells(row, colPartNumber).Value.ToString.Trim(" ")
            Dim itemNumber = wsControlSystem.Cells(row, colItemNumber).Value.ToString.Trim(" ")
            Dim partDestFolderName = String.Format("{0:00}", row) + " " + part
            Dim man = wsControlSystem.Cells(row, colManufacturer).Value.ToString.Trim(" ")
            'Dim partSourceFolderName = ""
            'Dim partExists = False
            updateStatus("Processing Part: " + part)

            ' Test getting PDF links
            Dim pdfLinks As ArrayList = getPDFlinks(itemNumber)

            ' If there are no links, skip this part
            If pdfLinks.Count = 0 Then
                updateStatus("No documents listed for: " + part)
                updateStatus("")
                progressBarGenerateProgress.Value += progressIncriment
                row = row + 1
                Continue While
            End If
            ' Create the folder for the manufacturer if it doesn't exist
            If Not Directory.Exists(manualDestDir + "\" + man) Then
                Directory.CreateDirectory(manualDestDir + "\" + man)
            End If
            ' Create the folder for the part if it doesn't exist
            If Not Directory.Exists(manualDestDir + "\" + man + "\" + partDestFolderName) Then
                Directory.CreateDirectory(manualDestDir + "\" + man + "\" + partDestFolderName)
            End If

            ' Loop through each link, copy pdf at link to folder
            For Each link As String In pdfLinks
                Try
                    My.Computer.FileSystem.CopyFile(manualSrcFilesDir + "\" + link, manualDestDir + "\" + man + "\" + partDestFolderName + "\" + Path.GetFileName(link))
                Catch ex As Exception
                    updateStatus(ex.Message)
                End Try
            Next

            updateStatus("")
            progressBarGenerateProgress.Value += progressIncriment
            row = row + 1
        End While
        progressBarGenerateProgress.Value = 100

        'Open generated manual
        Process.Start(manualDestDir)
    End Sub

    Private Function getPDFlinks(ByVal itemNumber As String) As ArrayList
        Dim pdfLinks As New ArrayList
        Dim sql As String = "SELECT path FROM thermal_economy.parts_pdfs WHERE item_number = '" + itemNumber + "';"
        Dim da As MySqlDataAdapter = doQuerry(sql)
        Dim dt As DataTable = New DataTable()
        da.Fill(dt)

        For Each row As DataRow In dt.Rows
            pdfLinks.Add(row(0).ToString())
        Next

        Return pdfLinks
    End Function

    Private Function doQuerry(ByVal querry As String) As MySqlDataAdapter
        Using connection As MySqlConnection = New MySqlConnection(My.Settings.ConString)
            Dim da As MySqlDataAdapter
            connection.Open()

            Using cmdSel As MySqlCommand = New MySqlCommand(querry, connection)
                da = New MySqlDataAdapter(cmdSel)
            End Using

            connection.Close()
            Return da
        End Using


    End Function

    'Private Sub generateOMManualOld()
    '    Dim progressIncriment As Decimal
    '    'Create folder for manual
    '    Dim curDir As String = Directory.GetCurrentDirectory()
    '    Dim manualDir As String = curDir + "\GeneratedManual"
    '    Dim manualFilesDir As String = curDir + "\ManualFiles"
    '    Directory.CreateDirectory(manualDir)

    '    'Analyze BOM
    '    'Dim excel As New Microsoft.Office.Interop.Excel.Application()
    '    wb = openBOM(excel)
    '    Dim wsControlSystem = getWS(excel, "Control System")
    '    Dim row As Integer = getPartNumberRow(wsControlSystem)
    '    Dim colPartNumber As Integer = getPartNumberColumn(wsControlSystem)
    '    Dim colManufacturer As Integer = getManufacturerColumn(wsControlSystem)
    '    progressIncriment = 1.3

    '    updateStatus("Reading parts from BOM...")
    '    While wsControlSystem.Cells(row, colPartNumber).value IsNot Nothing And wsControlSystem.Cells(row, colManufacturer).value IsNot Nothing
    '        Dim part = wsControlSystem.Cells(row, colPartNumber).Value.ToString.Trim(" ")
    '        Dim man = wsControlSystem.Cells(row, colManufacturer).Value.ToString.Trim(" ")
    '        updateStatus("Processing Part: " + part)
    '        If Directory.Exists(manualFilesDir + "\" + man + "\" + part) Then
    '            If Not Directory.Exists(manualDir + "\" + man) Then
    '                Directory.CreateDirectory(manualDir + "\" + man)
    '            End If
    '            My.Computer.FileSystem.CopyDirectory(manualFilesDir + "\" + man + "\" + part, manualDir + "\" + man + "\" + part)

    '            ' Copy any loose files in the manufacturer folder into the part folder
    '            Dim looseFiles = My.Computer.FileSystem.GetFiles(manualFilesDir + "\" + man + "\")
    '            For Each looseFile In looseFiles
    '                My.Computer.FileSystem.CopyFile(looseFile, manualDir + "\" + man + "\" + part + "\" + Path.GetFileName(looseFile))
    '            Next looseFile

    '            updateStatus("Document found for: " + part)
    '        Else
    '            updateStatus("No document found for: " + part)
    '        End If
    '        updateStatus("")
    '        progressBarGenerateProgress.Value += progressIncriment
    '        row = row + 1
    '    End While
    '    progressBarGenerateProgress.Value = 100

    '    'Close BOM
    '    wb.Close(False)
    '    excel.Quit()

    '    'Open generated manual
    '    Process.Start(manualDir)
    'End Sub

    Private Function getPartNumberColumn(ws As Microsoft.Office.Interop.Excel.Worksheet) As Integer
        Dim result = ws.Range("A1", "G15").Find("Part Number")
        If result Is Nothing Then
            MsgBox("Error. Could not find Part Number column in Excel sheet. Please verify column name.")
            System.Environment.Exit(2)
        End If
        Return result.Column
    End Function
    Private Function getItemNumberColumn(ws As Microsoft.Office.Interop.Excel.Worksheet) As Integer
        Dim result = ws.Range("A1", "G15").Find("TEI Item #")
        If result Is Nothing Then
            MsgBox("Error. Could not find Item Number column in Excel sheet. Please verify column name.")
            System.Environment.Exit(2)
        End If
        Return result.Column
    End Function

    Private Function getPartNumberRow(ws As Microsoft.Office.Interop.Excel.Worksheet) As Integer
        Dim result = ws.Range("A1", "G15").Find("Part Number")
        If result Is Nothing Then
            MsgBox("Error. Could not find Part Number column in Excel sheet. Please verify column name.")
            System.Environment.Exit(2)
        End If
        Return result.Row + 1
    End Function

    Private Function getManufacturerColumn(ws As Microsoft.Office.Interop.Excel.Worksheet) As Integer
        Dim result = ws.Range("A1", "G15").Find("Manufacturer")
        If result Is Nothing Then
            MsgBox("Error. Could not find Manufacturer column in Excel sheet. Please verify column name.")
            System.Environment.Exit(2)
        End If
        Return result.Column
    End Function

    Private Function getProgressIncriment(ws As Microsoft.Office.Interop.Excel.Worksheet, row As Integer) As Integer
        Return ws.Range("C5", "C500").Cells.SpecialCells(excel.XlCellType.xlCellTypeConstants).Count
    End Function

    Private Function openBOM(ByRef excel As Microsoft.Office.Interop.Excel.Application) As Microsoft.Office.Interop.Excel.Workbook
        Dim wb As Microsoft.Office.Interop.Excel.Workbook = excel.Workbooks.Open(textBoxChooseBOM.Text, , True) 'Open as read-only
        Return wb
    End Function

    Private Function getWS(ByRef excel As Microsoft.Office.Interop.Excel.Application, ByVal sheetName As String) As Microsoft.Office.Interop.Excel.Worksheet
        Dim ws As Microsoft.Office.Interop.Excel.Worksheet = Nothing
        Try
            ws = TryCast(excel.Worksheets(sheetName), Microsoft.Office.Interop.Excel.Worksheet)
        Catch ex As System.Runtime.InteropServices.COMException
            MsgBox("Correct tab could not be found. Make sure this is a BOM" + vbCrLf + ex.Message)
            excel.Quit()
            System.Environment.Exit(2)
        End Try

        Return ws
    End Function

    Public Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(obj)
            obj = Nothing
        Catch ex As System.Exception

            System.Diagnostics.Debug.Print(ex.ToString())
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Sub AllowUIToUpdate()

        Dim frame As New DispatcherFrame()

        Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Render, New DispatcherOperationCallback(Function(parameter As Object)


                                                                                                                frame.[Continue] = False


                                                                                                                Return Nothing

                                                                                                            End Function), Nothing)

        Dispatcher.PushFrame(frame)

    End Sub

    Private Sub Application_Exit(sender As Object, e As ExitEventArgs) Handles Me.Closed
        gracefulClose()
    End Sub

    Private Sub MenuItem_Click(sender As Object, e As RoutedEventArgs)
        Dim settingWindow As SettingsWindow = New SettingsWindow()
        settingWindow.Init()
        settingWindow.ShowDialog()
    End Sub
End Class

