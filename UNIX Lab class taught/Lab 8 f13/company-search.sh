#!/bin/bash

usage() { echo "Usage: $0 [-a <100|...|999>] [-s <string>] [-z <int>]" 1>&2; exit 1; }


STATE=0
ZIP_CODE=0
AREA_CODE=0

PRINT_STATE=0
PRINT_ZIP=0
PRINT_AREACODE=0

echo "Results should meet the following criteria:"

while getopts ":ASZa:s:z:" opt; do
  case $opt in
    a)
      echo " - Companie(s) have area code $OPTARG" >&2
      AREA_CODE=$OPTARG
      ;;
    s)
      echo " - Companie(s) are located in the state of $OPTARG" >&2
      STATE=$OPTARG
      ;;
    z)
      echo " - Companie(s) are located in zip code $OPTARG" >&2
      ZIP_CODE=$OPTARG
      ;;
    A)
      echo " - Output area code" >&2
      PRINT_AREACODE=1
      ;;
    S)
      echo " - Output State" >&2
      PRINT_STATE=1
      ;;
    Z)
      echo " - Output zip code" >&2
      PRINT_ZIP=1
      ;;
    \?)
      usage
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done
echo $STATE
awk -f ./companies.awk -v S=$STATE -v Z=$ZIP_CODE -v A=$AREA_CODE -v AP=$PRINT_AREACODE -v ZP=$PRINT_ZIP -v SP=$PRINT_STATE < companies.csv
