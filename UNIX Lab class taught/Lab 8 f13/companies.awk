BEGIN {
 
FS=",";

header="Name"
match_state=0
match_zip=0
match_areacode=0

if(SP!=0) header = header",State"
if(AP!=0) header = header",Area Code"
if(ZP!=0) header = header",Zip Code"

print header
}
# start main loop
{
	row=$1;

	if($4==S){
		if(SP == 1){ 
			row = row","S;
		}

		match_state = 1 ;
	}

	if($6 ~ /\([0-9]\)/ == A){
		if(AP==1) row = row","A;
		match_areacode = 1;
	}

	if($5 == Z){
		if(ZP==1) row = row","Z;
		match_zip = 1;
	}

	if (	(match_state==1 || S==0) && 
			(match_zip==1 || Z==0) && 
			(match_areacode==1 || A==0)	) {
		print row;
	}

	match_state=0;
	match_zip=0;
	match_areacode=0;

}# finish main loop
END{
	print " - DONE -" 
}