#!/bin/bash
#
#
#
#	usage: ./testrun.sh min_array_size max_array_size array_size_step max_num_threads
#
#
########################################
#NUM_RUNS=$1
THREAD_CT_LOW=2
ARR_LOW=$1
ARR_HIGH=$2
ARR_STEP_SIZE=$3
THREAD_CT_HIGH=$4
PARALLEL_ONLY=1

ARR_SIZE=$ARR_LOW
NUM_THREADS=$THREAD_CT_LOW

while [ $ARR_SIZE -le $ARR_HIGH ]
do
	while [ $NUM_THREADS -le $THREAD_CT_HIGH ]
	do	
		if ((NUM_THREADS <= 4 || NUM_THREADS >= 8))
			then
				./omm $ARR_SIZE $NUM_THREADS 1 >> testruns_omp_$ARR_SIZE.csv
		fi
		NUM_THREADS=$(( $NUM_THREADS * 2 ))
	done
	NUM_THREADS=2
	ARR_SIZE=$(( $ARR_SIZE + $ARR_STEP_SIZE ))
done